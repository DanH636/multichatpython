from my_client import MyClient
import my_UI
import sys


def client_work(client):
    client.connect()
    active = True
    while active:
        text = input()
        if(text == "#close"):
            active = False
        elif(text == "#getclients"):
            print(client.token.get_clients())
        else:
            dest, mess = text.split("->")
            if dest == "all":
                client.send_all(mess)
            else:
                client.send(dest, mess)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "GUI":
            my_UI.start_GUI()
        elif sys.argv[1] == "CLI" and len(sys.argv) > 2:
            if len(sys.argv) == 5:
                client = MyClient(sys.argv[2], sys.argv[3], int(sys.argv[4]))
                client_work(client)
            if len(sys.argv) == 4:
                client = MyClient(sys.argv[2], sys.argv[3])
                client_work(client)
            if len(sys.argv) == 3:
                client = MyClient(sys.argv[2])
                client_work(client)
        else:
            print("Number of argumments should be 3 or greater!!!")
    else:
        print("Number of argumments should be 2 or greater!!!")
